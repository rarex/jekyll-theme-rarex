# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-rarex"
  spec.version       = "0.1.0"
  spec.authors       = ["Terence Ponce"]
  spec.email         = ["terence@rarex.xyz"]

  spec.summary       = "Jekyll theme used by Rarex - A cryptocurrency exchange."
  spec.homepage      = "https://gitlab.com/rarex/jekyll-theme-rarex"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 3.8"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12.0"
end
